import discord
from discord.ext import commands


class Commands:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def about(self):

        embed=discord.Embed(title="Hello, Jane here!", description="I am Jane and i love to listen to some banging tunes, why don't you come and listen to some tunes with me mate", color=0x800000)
        embed.set_thumbnail(url='https://autowow.co.uk/botimg/jane.png')
        embed.set_footer(text="Jane - Bot made by WoW#0001 as a gift for his pal C21.exe#3313")

        await self.bot.say(embed=embed)

def setup(bot):
    bot.add_cog(Commands(bot))
    print('About is loaded')
