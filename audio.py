import asyncio
import discord
from discord.ext import commands
if not discord.opus.is_loaded():
    discord.opus.load_opus('opus')

def __init__(self, bot):
        self.bot = bot

class VoiceEntry:
    def __init__(self, message, player):
        self.requester = message.author
        self.channel = message.channel
        self.player = player

    def __str__(self):
        fmt = '\n**Title:** {0.title}\n**Uploaded by:** {0.uploader}\n**Added by:** {1.display_name}'
        duration = self.player.duration
        if duration:
            fmt = fmt + '\n**Length:** {0[0]}**m** {0[1]}**s**'.format(divmod(duration, 60))
        return fmt.format(self.player, self.requester)

class VoiceState:
    def __init__(self, bot):
        self.current = None
        self.voice = None
        self.bot = bot
        self.play_next_song = asyncio.Event()
        self.songs = asyncio.Queue()
        self.skip_votes = set()
        self.audio_player = self.bot.loop.create_task(self.audio_player_task())

    def is_playing(self):
        if self.voice is None or self.current is None:
            return False

        player = self.current.player
        return not player.is_done()

    @property
    def player(self):
        return self.current.player

    def skip(self):
        self.skip_votes.clear()
        if self.is_playing():
            self.player.stop()

    def toggle_next(self):
        self.bot.loop.call_soon_threadsafe(self.play_next_song.set)

    async def audio_player_task(self):
        while True:
            self.play_next_song.clear()
            self.current = await self.songs.get()
            self.current.player.start()
            await self.play_next_song.wait()
class Commands:

    def __init__(self, bot):
        self.bot = bot
        self.voice_states = {}

    def get_voice_state(self, server):
        state = self.voice_states.get(server.id)
        if state is None:
            state = VoiceState(self.bot)
            self.voice_states[server.id] = state

        return state

    async def create_voice_client(self, channel):
        voice = await self.bot.join_voice_channel(channel)
        state = self.get_voice_state(channel.server)
        state.voice = voice

    def __unload(self):
        for state in self.voice_states.values():
            try:
                state.audio_player.cancel()
                if state.voice:
                    self.bot.loop.create_task(state.voice.disconnect())
            except:
                pass

    @commands.command(pass_context=True, no_pm=True)
    async def summon(self, ctx):
        """Ask Jane to join a voice channel"""
        summoned_channel = ctx.message.author.voice_channel
        if summoned_channel is None:
            await self.bot.say(':x: **M8 your not even in a voice channel**')
            return False

        state = self.get_voice_state(ctx.message.server)
        if state.voice is None:
            state.voice = await self.bot.join_voice_channel(summoned_channel)
        else:
            await state.voice.move_to(summoned_channel)

        return True

    @commands.command(pass_context=True, no_pm=True)
    async def play(self, ctx, *, song : str):
        """Play a sweet tune"""
        state = self.get_voice_state(ctx.message.server)
        opts = {
            'default_search': 'auto',
            'quiet': True,
        }

        if state.voice is None:
            success = await ctx.invoke(self.summon)
            await self.bot.say(":mag_right: **Searching song**")
            if not success:
                return

        try:
            player = await state.voice.create_ytdl_player(song, ytdl_options=opts, after=state.toggle_next)
        except Exception as e:
            fmt = ':x: **RIP** ```REASON: py\n{}: {}\n```'
            await self.bot.send_message(ctx.message.channel, fmt.format(type(e).__name__, e))
        else:
            player.volume = 1
            entry = VoiceEntry(ctx.message, player)

            embedplay=discord.Embed(title="**Added to Queue :white_check_mark: **", color=0x800000)
            await self.bot.say(embed=embedplay)
            await state.songs.put(entry)

    @commands.command(pass_context=True, no_pm=True)
    async def stop(self, ctx):
        """Stop music and clear the queue"""
        server = ctx.message.server
        state = self.get_voice_state(server)

        if state.is_playing():
            player = state.player
            player.stop()

        try:
            state.audio_player.cancel()
            del self.voice_states[server.id]
            await state.voice.disconnect()
            embedstop=discord.Embed(title="**:white_check_mark: **Queue Cleared and stopped the music!**", color=0x800000)
            await self.bot.say(embed=embedstop)
        except:
            pass

    @commands.command(pass_context=True, no_pm=True)
    async def skip(self, ctx):
        """Vote for a skip or instant skip"""
        state = self.get_voice_state(ctx.message.server)
        if not state.is_playing():
            embedskip=discord.Embed(title=":x: **Nothing is on right now**", color=0x800000)
            await self.bot.say(embed=embedskip)
            return

        voter = ctx.message.author
        if voter == state.current.requester:
            embedskip2=discord.Embed(title=":white_check_mark: **Requester wanted to skip the song**", color=0x800000)
            await self.bot.say(embed=embedskip2)
            state.skip()
        elif voter.id not in state.skip_votes:
            state.skip_votes.add(voter.id)
            total_votes = len(state.skip_votes)
            if total_votes >= 3:
                embedskip3=discord.Embed(title=":white_check_mark: **Total Number of skips reached!**", color=0x800000)
                await self.bot.say(embed=embedskip3)
                await self.bot.say('')
                state.skip()
            else:
                embedskip4=discord.Embed(title=":white_check_mark: **You have voted to skip!**\n**Skips:** {}/3".format(total_votes), color=0x800000)
                await self.bot.say(embed=embedskip4)
        else:
            embedskip5=discord.Embed(title=":x: **M8 you have already voted to skip!**", color=0x800000)
            await self.bot.say(embed=embedskip5)

    @commands.command(pass_context=True, no_pm=True)
    async def np(self, ctx):
        """Show what is playing"""
        state = self.get_voice_state(ctx.message.server)
        if state.current is None:
            await self.bot.say(':x: **Nothing is playing right now**')
        else:
            skip_count = len(state.skip_votes)
            embednp=discord.Embed(title="**Now Playing**",description="{}\n**Skips:** {}/3".format(state.current, skip_count), color=0x800000)
            embednp.set_thumbnail(url="https://autowow.co.uk/botimg/jane.png")
            await self.bot.say(embed=embednp)

def setup(bot):
    bot.add_cog(Commands(bot))
    print('Music is loaded')
